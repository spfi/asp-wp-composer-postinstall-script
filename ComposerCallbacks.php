<?php

namespace aspergerag\wpinstaller;

use Composer\Script\Event;


class ComposerCallbacks
{
	public static function postInstall(Event $event)
	{

		$delete_default = [
			"wp-content/themes/twentysixteen",
			"wp-content/themes/twentytwenty",
			"wp-content/themes/twentyseventeen",
			"wp-content/themes/twentynineteen",
			"wp-content/plugins/akismet",
			"wp-content/plugins/hello.php",
		];

		$delete_explicit = [
			"wp-content/plugins/akismet/.htaccess",
			"wp-content/themes/twentytwenty/.stylelintrc.json"
		];

		$extra = $event->getComposer()->getPackage()->getExtra();
		$keep = $extra["wordpress-keep"] ?? [];
		$remove = $extra["wordpress-remove"] ?? [];
		$remove_expl = $extra["wordpress-remove-explicit"] ?? [];
		$wp_path = $extra["wordpress-install-dir"] ?? "wordpress";

		foreach (array_merge($delete_explicit, $remove_expl) as $target) {
			@unlink("$wp_path/$target");
		}

		foreach (array_diff(array_merge($delete_default, $remove), $keep) as $target) {
			@ComposerCallbacks::delete_files("$wp_path/$target");
		}

		@copy("vendor/aspergerag/wpinstaller/src/gitignore-wp", "$wp_path/.gitignore");
	}


	// snippet from https://paulund.co.uk/php-delete-directory-and-files-in-directory
	/* 
	 * php delete function that deals with directories recursively
	 */
	static function delete_files($target)
	{
		if (is_dir($target)) {
			$files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

			foreach ($files as $file) {
				ComposerCallbacks::delete_files($file);
			}
			rmdir($target);
		} elseif (is_file($target)) {
			unlink($target);
		}
	}
}
